import json
import os.path

from herbie_api import OpenApi


def test_basic():
    open_api = OpenApi(None)
    data = open_api.load()
    assert data['openapi'] == '3.0.2'
    assert data['info']['title'] == 'Herbie'


def test_load():
    open_api = OpenApi(None)
    open_api.load()
    schema = open_api.get_protocol_schema(f"/api/data-post-modifications/{1}/")

    assert schema.params['reaction_status']['oneOf'][0]['$ref'] == '#/components/schemas/DataPostModificationReactionStatus'
    assert schema.draft['reaction_status']['oneOf'][0]['$ref'] == '#/components/schemas/DataPostModificationReactionStatus'



def test_common_name():
    open_api = OpenApi(None)
    open_api.load()
    schema = open_api.get_protocol_schema(f"/api/data-post-modifications/{1}/")
    mapping = schema.mapping
    assert mapping['old_product']['target'] == 'old_product_id'
    assert not mapping['old_product']['exact']
    assert mapping['old_product']['suffix'] == 'id'

    assert mapping['new_product_id']['exact']
    assert mapping['new_product_id']['target'] == 'new_product_id'

def test_parse_instance():
    open_api = OpenApi(None)
    open_api.load()
    schema = open_api.get_protocol_schema(f"/api/data-post-modifications/{1}/")

    with open( os.path.join(os.path.dirname(__file__), 'data/test_data.json'), 'r') as f:
        json_data = json.loads(f.read())
    data = open_api.parse_open_api(json_data, schema)
    pass