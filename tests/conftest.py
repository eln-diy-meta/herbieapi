import os
import configparser
import subprocess

from herbie_api import Instance as HI
import pytest


def pytest_sessionstart(session):
    global DUMP_PATH
    path = os.path.join(os.getcwd(), 'data/setup_herbie_db.sh')
    path_dir = os.path.join(os.getcwd(), 'data')

    process = subprocess.Popen(path, shell=True, executable='/bin/bash', cwd=path_dir,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    stdout, stderr = process.communicate()
    res_error = stderr.decode("utf-8").split('\n')
    res = stdout.decode("utf-8").split('\n')
    DUMP_PATH = res[-2]


@pytest.fixture(scope='session')
def CONFIG():
    config = configparser.ConfigParser()
    file = os.path.join(os.path.dirname(__file__), '../config.ini')
    config.read(file)
    yield config


@pytest.fixture(scope='session')
def herbie_instance(CONFIG):
    herbie_instance = HI(host_url=CONFIG['HERBIE']['host'], api_key=CONFIG['HERBIE']['api_key'],
                         verify_ssl=CONFIG.getboolean('HERBIE', 'verify_ssl'))
    yield herbie_instance
