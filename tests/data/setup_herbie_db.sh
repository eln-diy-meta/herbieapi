HERBIEPATH="${HERBIEPATH:-../../../herbie-dev}"
echo "Relative Herbie path: $HERBIEPATH"

cd $HERBIEPATH

if [ "$( PGPASSWORD=secret psql -h localhost -p 5432 -U admin herbie -XtAc "SELECT 1 FROM pg_database WHERE datname='herbie'" )" = '1' ]
then
    echo "Database available exists"
else
    docker-compose up -d
    sleep 2
    echo "Database not available pleas Check"
    exit 1
fi

BASEDIR=$(dirname $0)
source ./venv/bin/activate
timestamp=$(date +%Y_%m_%d_%H_%M_%S_%N)
mkdir -p dumps
python manage.py dumpdata --exclude auth.permission --exclude contenttypes  > $PWD/dumps/$timestamp.json
python manage.py flush --noinput
python manage.py loaddata --exclude auth.permission --exclude contenttypes $BASEDIR/db.json

echo $PWD/dumps/$timestamp.json