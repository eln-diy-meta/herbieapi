import uuid


def test_post_modification(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    p = herbie_instance.get_post_modification(9)
    p.to_draft()
    v = p.values['modifying_purification_solvents'].add_new()
    p.values['modifying_purification_solvents'][-1].values = {
        "purif_solvent": uuid.uuid4().__str__(),
        "purif_solvent_label": uuid.uuid4().__str__(),
        "purif_volume_ml": 22,
        "purif_volume_ratio_percent": 11
    }
    v = p.values['modifying_purification_solvents'].add_new()
    p.values['modifying_purification_solvents'][-1].values = {
        "purif_solvent": uuid.uuid4().__str__(),
        "purif_solvent_label": uuid.uuid4().__str__(),
        "purif_volume_ml": 33,
        "purif_volume_ratio_percent": 44
    }

    p.save_draft()
    p.values['modifying_purification_solvents'].remove(0)
    p.publish()
    pass