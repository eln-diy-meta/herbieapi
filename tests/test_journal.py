def test_load_journal(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    journal = herbie_instance.journal_entries
    assert len(journal) > 0


def test_find_in_journal(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    journal = herbie_instance.find_product(product_id_name= "ComPoly3$2")
    assert len(journal) == 1
    journal = herbie_instance.find_product(product_id_name= "ComPoly3$20-019;")
    assert len(journal) == 1
    journal = herbie_instance.find_product(product_id_name= "ComPoly3$20-019;xx")
    assert len(journal) == 0
