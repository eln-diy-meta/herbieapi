from datetime import date
import pytest

import herbie_api.protocol


def test_instance(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.update_token()
    herbie_instance.setup_env()


def test_load_post_modification(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    p: herbie_api.protocol.Protocol = herbie_instance.get_post_modification(9)
    if p.is_draft: p.publish()
    v = p.values['modifying_reagents'].add_new()
    v.values['rea_product'] = herbie_instance.get_product(11)
    with pytest.raises(Exception):
        v.save()
    p.to_draft()
    v = p.values['modifying_reagents'].add_new()
    v.values['rea_product'] = herbie_instance.get_product(10)
    v.save()
    p.delete()



def test_new_product(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    p = herbie_instance.create_product()
    p.meta['clients_author'] += herbie_instance.get_authors(username='martin')
    p.meta['clerks'] += herbie_instance.get_authors(username='martin')
    p.meta['clients_department'] += herbie_instance.get_departments(name_de="Funktionale Magnesiummaterialien")
    p.meta['performance_times'] += [{'date': date.today(), 'activity': 1}]
    p.values['comment'] = "This is a test commend"
    p.values['product_id_name'] = "TEST123qweTEST"
    p.save_draft()
    p.publish()

def test_load_product(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    p = herbie_instance.get_product(10)
    assert p.values['product_id_name'] == 'ComPoly3$20-019;'
    p.values['comment'] = 'Should not work!!'
    with pytest.raises(Exception):
        p.save_draft()
    p.to_draft()
    p.values['comment'] = 'Should work!!'
    p.save_draft()
    p.publish()




def test_post_modification(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    pr_TEST_a = herbie_instance.get_product(10)
    p = herbie_instance.create_post_modification()
    p.meta['clients_author'] += herbie_instance.get_authors(username='martin')
    p.meta['clerks'] += herbie_instance.get_authors(username='martin')
    p.meta['clients_department'] += herbie_instance.get_departments(name_de="Funktionale Magnesiummaterialien")
    p.meta['performance_times'] += [{'date': date.today(), 'activity': 1}]
    p.values['reaction_scheme'] = "https://community.sophos.com/resized-image/__size/320x240/__key/communityserver-discussions-components-files/126/1348.pastedimage1487785177396v17.png"
    p.values['old_product'] = pr_TEST_a.material
    p.values['new_product_id'] = 'NEW_PRODUCT'
    p.values['new_product_label'] = 'NEW_PRODUCT_LABEL'
    p.values['membrane_type'] = {'@id': 'flat-sheet'}
    p.values['condition_select'] = {'@id': 'microwave'}
    p.values['mass_mg'] = 99
    p.values['area_mm2'] = 22
    p.values['condition_text'] = "TEST CREATE"
    p.values['modifying_reagents'].add_new()
    a = p.values['modifying_reagents']
    p.save_draft()
    p.publish()

def test_author(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    all_user = herbie_instance.get_authors()
    martin = herbie_instance.get_authors(username='martin')
    assert len(martin) == 1
    assert len(all_user) == 2

def test_departments(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    all_user = herbie_instance.get_departments()
    MBF = herbie_instance.get_departments(name_de="Laden")
    assert len(MBF) == 1
    assert len(all_user) == 1
    
