def test_load_materials(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    materials = herbie_instance.get_materials()
    assert len(materials) >= 5

def test_load_filtered_materials(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    materials = herbie_instance.get_materials({'label_en': '11'})
    assert len(materials) == 1
    materials = herbie_instance.get_materials({'label_en': '11', 'label_de': '1ss1'})
    assert len(materials) == 0

def test_load_material(herbie_instance):
    assert herbie_instance.test_connection() is herbie_instance
    herbie_instance.setup_env()
    material = herbie_instance.get_material(label_en ='11')
    assert material.id == 9