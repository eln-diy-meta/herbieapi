import json
import os

from requests import Session
from unittest.mock import patch

from herbie_api import Instance as HI

with open( os.path.join(os.path.dirname(__file__), 'data/test_meta_data.json'), 'r') as f:
    json_meta_data = f.read()

with open( os.path.join(os.path.dirname(__file__), 'data/test_data.json'), 'r') as f:
    json_data = f.read()


class ResMock():
    def __init__(self, status_code, content):
        self.status_code = status_code
        self.content = content
    def json(self):
        return json.loads(self.content)

@patch.object(Session, 'get')
@patch.object(Session, 'post')
@patch.object(Session, 'put')
@patch.object(Session, 'patch')
def test_post_modification(mock_patch,mock_put, mock_post, mock_get,CONFIG):

    mock_post.return_value =  ResMock(201, json_meta_data)
    mock_get.return_value =  ResMock(200, json_data)
    mock_put.return_value =  ResMock(200, {})
    mock_patch.return_value =  ResMock(200, {})
    herbie_instance = HI(host_url=CONFIG['HERBIE']['host'], api_key=CONFIG['HERBIE']['api_key'], verify_ssl=CONFIG.getboolean('HERBIE', 'verify_ssl'))
    p = herbie_instance.create_protocol('post-modification')

    mock_get.return_value =  ResMock(200, json_meta_data)
    p.save_draft()




