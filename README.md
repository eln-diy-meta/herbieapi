# HerbieAPI

A Python API for Herbie. Herbie is an Electronic Laboratory Notebook (ELN) developed as a web-based platform designed to help researchers manage and organize their experimental data in a secure and efficient manner.

The HerbieApi Python package is a wrapper for the RESTful API built on top of the Ruby on Rails framework that allows users to interact with Herbie ELN. The package provides a simple and intuitive interface for creating, retrieving, updating, and deleting data stored in the ELN using Python. With its RESTful architecture and comprehensive documentation, the HerbieApi allows users to build custom applications and workflows that leverage the ELN's capabilities and streamline their research processes.