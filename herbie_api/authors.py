from herbie_api import Connection
from requests.exceptions import RequestException

class AuthorManager:

    @classmethod
    def get_me(cls, connection: Connection) -> dict:
        res = connection.get('/api/me/author/')
        if res.status_code != 200:
            raise RequestException("Author You could not be fetched!")
        return res.json()

    @classmethod
    def get_departments(cls, connection: Connection, **kwargs) -> list[dict]:
        res = connection.get('/api/departments/')
        if res.status_code != 200:
            raise RequestException("Departments could not be fetched!")
        return [x for x in res.json() if cls._filter(x, kwargs)]
    @classmethod
    def get_authors(cls, connection: Connection, **kwargs) -> list[dict]:
        res = connection.get('/api/authors/')
        if res.status_code != 200:
            raise RequestException("Authors could not be fetched!")
        return [x for x in res.json() if cls._filter(x.get('user', {}), kwargs)]

    @classmethod
    def _filter(cls, obj :dict, kwargs: dict) -> bool:
        for (k, v) in kwargs.items():
            if obj.get(k) != v:
                return False
        return True