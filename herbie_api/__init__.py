import requests

from herbie_api.connection import Connection
from herbie_api.journal import Journal
from herbie_api.materials import Material
from herbie_api.open_api import OpenApi
from herbie_api.protocol import Protocol
from herbie_api.authors import AuthorManager


class Instance:
    """ The herbie_instance object is the core object of the Herbie API. In order for the API to work,
    a connection to a Herbie (server-)herbie_instance must first be established.
    an Instance object manges such a connection. To initializes an herbie_instance it needs
     the host URL of the Herbie server as a string and the API key.

    :param host_url: URL for the new :class:`Request` object
    :param api_key: authentification key which can be generated in the Herbie deshboard

    Usage::

    >>> from herbie_api import Instance
    >>> from  requests.exceptions import ConnectionError as CE
    >>> import logging
    >>> try:
    >>>     s = Instance('http(d)://xxx.xxx.xxx', '<API_KEY>').test_connection()
    >>> except CE as e:
    >>>     logging.error(f"A connection to Chemotion ({s.host_url}) cannot be established")


    """

    def __init__(self, host_url: str, api_key: str, verify_ssl: bool):
        self._is_setup = False
        self._protocols = []
        self.host_url = host_url
        self.connection = Connection(host_url, api_key, verify_ssl=verify_ssl)
        self.open_api = OpenApi(self.connection)
        self._journal = Journal(self.connection, self.open_api)

    def setup_env(self, hard=False):
        if self._is_setup:
            if not hard:
                return
            else:
                for prod_key in self._protocols:
                    delattr(self, f"create_{prod_key}")
                    delattr(self, f"get_{prod_key}")
                    delattr(self, f"find_{prod_key}")
                self._protocols = []

        def set_handler(protocol_type):
            setattr(self, f"create_{prod_key.replace('-', '_')}", lambda: self.create_protocol(protocol_type))
            setattr(self, f"get_{prod_key.replace('-', '_')}", lambda id: self.get_protocol(protocol_type, id))
            setattr(self, f"find_{prod_key.replace('-', '_')}",
                    lambda **kwargs: self.find_protocol(protocol_type, kwargs))

        self.open_api.load(hard)
        for (prod_key, prod_name) in self.open_api.components['ProtocolData']['discriminator']['mapping'].items():
            self._protocols.append(prod_key)
            set_handler(prod_key)

    @property
    def journal_entries(self):
        return self._journal.entries

    @property
    def journal_notes(self):
        return self._journal.notes

    @property
    def api_token(self):
        return self.connection.api_token

    def get_me(self) -> dict:
        """
        Returns you as Author objects

        :return: you as a authors
        """
        return AuthorManager.get_me(self.connection)

    def get_authors(self, **kwargs) -> list[dict]:
        """
        Returns a list of Author objects. The kwargs can be used to filter the list of authors.
        Note that the filter refers user properties as username, email, fist_name, last_name and so on.

        :param kwargs: User properties as filter values as unsername=xxx for herbie_instance
        :return: Filtered list of authors
        """
        return AuthorManager.get_authors(self.connection, **kwargs)

    def get_material(self, **filter) -> None | Material:
        res = self.get_materials(filter)
        if len(res) == 0:
            return None
        return res[0]

    def get_materials(self, filter: dict | None = None) -> list[Material]:
        """
        Returns a list of material objects.

        :return: Filtered list of authors
        :raises ConnectionError (requests.exceptions.ConnectionError) if the connection cannot be established.
        """
        mats = Material.load_materials(self.connection)
        if filter is None:
            return mats
        filtered_mats: list[Material] = []
        for mat in mats:
            does_fit = True
            for key, val in filter.items():
                if mat.values.get(key) != val:
                    does_fit = False
                    continue
            if does_fit:
                filtered_mats.append(mat)
        return filtered_mats

    def get_departments(self, **kwargs):
        """
        Returns a list of Department objects. The kwargs can be used to filter the list of departments.
        Note that the filter refers department properties as name_en, name_de, fist_name and abbreviation.

        :param kwargs: User properties as filter values as unsername=xxx for herbie_instance
        :return: Filtered list of authors
        """
        return AuthorManager.get_departments(self.connection, **kwargs)

    def test_connection(self):
        """
        This test_connection methode simply test if the connection to a Herbie herbie_instance can be established.

        :return: the herbie_instance itself

        :raises ConnectionError (requests.exceptions.ConnectionError) if the connection cannot be established.
        """

        res = self.connection.get('/api/me/', verify=self.connection.verify_ssl)
        if res.status_code != 200:
            raise requests.exceptions.ConnectionError(
                f'Connection failed! {self.host_url}: ({res.status_code})\n{res.text}')
        return self

    def update_token(self):
        """
        This update_token methode fetches a new token.

        :return: the herbie_instance itself
        """
        self.connection.update_token(self.get_me())
        return self

    def create_protocol(self, protocol_type: str) -> Protocol:
        """
        This create_protocol methode creates a new empty product of a given type.
        Calling this function automatically creates a draft object on the server.

        :param protocol_type: The type of the protocol.

        :return: Protocol object connected to a new draft on the server
        """

        p = Protocol(self.connection, protocol_type, self.open_api)
        p.create()
        return p

    def get_protocol(self, protocol_type: str, id: int) -> Protocol:
        """
        This get_protocol methode fetches a protocol of a given type.

        :param protocol_type: The type of the protocol.
        :param id: The database ID of the protocol

        :return: Product object connected to a new draft on the server
        """

        p = Protocol(self.connection, protocol_type, self.open_api)
        p.set_id(id)
        p.load()
        return p

    def find_protocol(self, protocol_type: str, find_obj: dict) -> list[Protocol]:
        """
        This get_protocol methode fetches a protocol of a given type.

        :param protocol_type: The type of the protocol.
        :param find_obj: dictionary with your search parameter. for example: {'name': 'SOME_NAME'}

        :return: Product object connected to a new draft on the server
        """

        results = [x for x in self.journal_entries if x.protocol_type == protocol_type]
        for key, val in find_obj.items():
            results = [x for x in results if x.values.get(key) == val or x.meta.get(key) == val]

        return results
