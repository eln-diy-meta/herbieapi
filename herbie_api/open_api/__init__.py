import json
import os
import re
import urllib.parse
from datetime import datetime

import requests
from herbie_api.connection import Connection
from herbie_api.open_api.schema import ProtocolSchema
from herbie_api.materials import EmptyMaterial


class OpenApi:
    api_json_file_path = os.path.join(os.path.dirname(__file__), 'open_api.json')

    def __init__(self, connection: Connection):
        self._connection = connection
        self._data: dict | None = None

    @property
    def components(self) -> dict:
        return self.load(False)['components']['schemas']

    def get_component(self, protocol_path: str) -> dict:
        protocol_path = protocol_path.removeprefix('#/components/schemas/')
        return self.components.get(protocol_path, {}) | {'type': protocol_path}

    def get_protocol_schema(self, url: str) -> ProtocolSchema:
        url_path = urllib.parse.urlparse(url).path.split('/')
        urls = [x.split('/') for x in self.load()['paths'].keys()]
        urls = [x for x in urls if len(x) == len(url_path)]
        variable_matcher = re.compile('^\{.+}$')
        for idx, elem in enumerate(url_path):
            urls = [x for x in urls if x[idx] == elem or variable_matcher.match(x[idx]) is not None]
        if len(urls) != 1:
            raise ValueError(f"Url {url} is no valid API-path key")
        params_path = \
            self.load()['paths']['/'.join(urls[0])]["put"]["requestBody"]["content"]["application/json"]["schema"][
                "$ref"]
        draft_path = self._get_draft_from_params(params_path)

        return ProtocolSchema(draft_schema=self.get_component(draft_path), params_schema=self.get_component(params_path))

    def get_protocol_schema_from_draft_name(self, draft_name: str) -> ProtocolSchema:
        params_name = self._get_params_from_draft(draft_name)
        return ProtocolSchema(draft_schema=self.get_component(draft_name), params_schema=self.get_component(params_name))

    def load(self, hard: bool = False) -> dict:
        if not hard and self._data is not None:
            return self._data
        if hard or not os.path.exists(self.api_json_file_path):
            self._data = self._load_from_server()
        else:
            self._data = self._load_from_file()
        return self._data

    def _load_from_server(self):
        res = self._connection.get(url_path='/api/schema/?format=openapi-json')
        if res.status_code != 200:
            raise requests.exceptions.ConnectionError(f'Connection failed! {self._connection.host_url}')

        with open(self.api_json_file_path, 'w') as f:
            f.write(res.text)
        return res.json()

    def _load_from_file(self):
        with open(self.api_json_file_path, 'r') as f:
            return json.loads(f.read())

    def parse_open_api(self, json_values, schema: ProtocolSchema):
        if json_values is None:
            return None
        values = {}
        if 'type' in schema.draft and  schema.draft['type'] != 'object':
            return self._parse_single_value(json_values, schema.params, schema.draft)
        else:
            for k, v in schema.mapping.items():
                values[k] = self._parse_single_value(json_values.get(k, None), schema.params.get(v['target']), schema.draft.get(k))
        return values

    def _get_draft_from_params(self, params_path: str) -> str:
        if params_path.endswith('Params'):
            return params_path.removesuffix('Params') + 'Draft'
        return params_path

    def _get_params_from_draft(self, params_path: str) -> str:
        if params_path.endswith('Draft'):
            return params_path.removesuffix('Draft') + 'Params'
        return params_path

    def _handle_one_of(self, value, params_schema: dict, draft_schema: dict):

        for (idx, option) in enumerate(draft_schema['oneOf']):
            try:
                params = params_schema['oneOf'][idx] if 'oneOf' in params_schema else params_schema
                return self._parse_single_value(value, params, option)
            except:
                pass

        raise ValueError('value is not None')

    def _parse_single_value(self, json_value: str, params_schema, draft_schema) -> any:

        is_allow_null = draft_schema.get('type') == 'null'


        if 'oneOf' in draft_schema:
            if len(draft_schema['oneOf']) == 2 and draft_schema['oneOf'][1]['type'] == 'null':
                draft_schema = draft_schema['oneOf'][0]
                params_schema =  params_schema['oneOf'][0] if 'oneOf' in params_schema else params_schema
                is_allow_null = True
            else:
                return self._handle_one_of(json_value, params_schema, draft_schema)

        if not is_allow_null and json_value is None:
            if draft_schema.get('type') == 'string':
                return ''
            elif '$ref' in draft_schema:
                if draft_schema['$ref'] == '#/components/schemas/MaterialNested':
                    return EmptyMaterial()
            else:
                return None
            #raise ValueError('value is not None')
        if '$ref' in draft_schema:
            sub_schema = ProtocolSchema(self.get_component(draft_schema["$ref"]),
                                        self.get_component(self._get_params_from_draft(draft_schema["$ref"])))
            if sub_schema.check_if_protocol_obj():
                return {'herbie_api_sub_schema' : sub_schema, 'data': json_value}
            return self.parse_open_api(json_value, sub_schema)
        elif 'type' in draft_schema:
            if json_value is None:
                return None
            if draft_schema['type'] == 'object':
                if 'properties' not in draft_schema:
                    return json_value
                sub_schema = ProtocolSchema(draft_schema, params_schema)
                return self.parse_open_api(json_value, sub_schema)
            if draft_schema['type'] == 'array':
                item_ds = draft_schema['items']
                item_ps = params_schema.get('items', item_ds)
                if '$ref' in item_ds:
                    sub_schema = ProtocolSchema(self.get_component(item_ds["$ref"]),
                                            self.get_component(self._get_params_from_draft(item_ds["$ref"])))
                    if sub_schema.check_if_protocol_obj():
                        return {'herbie_api_sub_schema' : sub_schema, 'data': json_value}

                return [self._parse_single_value(x, item_ps, item_ds) for x in json_value]
            if draft_schema['type'] == 'integer':
                return int(json_value)
            if draft_schema['type'] == 'boolean':
                return bool(json_value)
            elif draft_schema['type'] == 'string':
                if draft_schema.get('format') == 'decimal':
                    return float(json_value)
                if draft_schema.get('format') == 'date':
                    return datetime.strptime(json_value, "%Y-%m-%d").date()

                return json_value
        return json_value
        #raise ValueError('Value could not be parsed')
