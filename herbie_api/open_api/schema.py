import logging

class ProtocolSchema:

    def __init__(self, draft_schema: dict, params_schema: dict):
        self.draft = draft_schema.get('properties', None)
        self._draft_name = draft_schema['type']
        self.params = params_schema.get('properties', self.draft)
        self._params_name = params_schema['type']
        self._mapping = None


    @property
    def parmas_name(self):
        return self._params_name

    @property
    def draft_name(self):
        return self._draft_name

    @property
    def mapping(self) -> dict[str, dict[str, str|bool]]:
        if self._mapping is None:
            self._mapping = self._find_key_mapping()
        return self._mapping

    def check_if_protocol_obj(self):
        return self._draft_name.endswith('Draft') and self._params_name.endswith('Params')
        

    def _check_if_sub_element(self, draft):
        if isinstance(draft, list):
            for draft_elem in draft:
                res = self._check_if_sub_element(draft_elem)
                if res is not None:
                    return res

        if isinstance(draft, dict):
            if '$ref' in draft:
                return draft['$ref'].removeprefix('#/components/schemas/')
            elif 'oneOf' in draft:
                return self._check_if_sub_element(draft['oneOf'])
        return None


    def _find_key_mapping(self) -> dict[str, dict[str, str | bool]]:
        keys : dict[str, dict[str, str]]= {}
        for key in self.params.keys():
            if self.draft is None or key in self.draft:
                keys[key] = {'target': key, 'exact': True}
            else:
                key_list = key.split('_')
                new_key = '_'.join(key_list[:-1])
                if new_key in self.draft:
                    draft = self._check_if_sub_element(self.draft[new_key])
                    keys[new_key] = {'target': key, 'exact': False, 'suffix': key_list[-1]}
                    if draft is not None:
                        keys[new_key]['sub_element'] = draft
                else:
                    logging.info(f"The key: {key} in the openapi-params object could not be matched")

        return keys
