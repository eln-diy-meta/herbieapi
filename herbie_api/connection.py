import datetime
import json

import requests


class Connection(requests.Session):
    def __init__(self, host_url: str, api_key: str, verify_ssl: bool):
        super().__init__()
        self._host_url = host_url.strip('/')
        self._api_key = api_key
        self._verify = verify_ssl
        self._csrftoken = None

    @property
    def host_url(self):
        return self._host_url

    @property
    def verify_ssl(self):
        return self._verify

    @property
    def api_token(self):
        return self._api_key

    @property
    def csrf_token(self):
        if self._csrftoken is None:
            self.get('')
            if 'csrftoken' in self.cookies:
                self._csrftoken = self.cookies['csrftoken']
            else:
                self._csrftoken = self.cookies['csrf']

        return self._csrftoken

    def update_token(self, user):
        res = self.post('/api/auth/login/', data={
            'expiry': (datetime.datetime.now() + datetime.timedelta(days=1)).isoformat(),
            'token': self._api_key.lstrip('Token '),
            'user': user.get('user')
        })
        if res.status_code != 200:
            raise requests.exceptions.ConnectionError(f'Fetching Token failed! {self.host_url}: ({res.status_code})\n{res.text}')
        self._api_key = f'Token {res.json()["token"]}'

    def get(self, url_path: str = '', **kwargs) -> requests.Response:
        return self._send_request('get', url_path, self.get_default_session_header(), kwargs)

    def post(self, url_path: str = '', **kwargs) -> requests.Response:
        return self._send_request('post', url_path, self.get_post_session_header(), kwargs)

    def patch(self, url_path: str = '', **kwargs) -> requests.Response:
        return self._send_request('patch', url_path, self.get_post_session_header(), kwargs)

    def delete(self, url_path: str = '', **kwargs) -> requests.Response:
        return self._send_request('delete', url_path, self.get_post_session_header(), kwargs)

    def put(self, url_path: str = '', **kwargs) -> requests.Response:
        return self._send_request('put', url_path, self.get_post_session_header(), kwargs)

    def _send_request(self, method: str, url_path: str, default_header: dict, kwargs: dict) -> requests.Response:
        kwargs['verify'] = kwargs.get('verify', self._verify)
        kwargs['url'] = kwargs.get('url', f"{self._host_url}/{url_path.lstrip('/')}")
        kwargs['headers'] = kwargs.get('headers', default_header)
        if 'data' in kwargs and not isinstance(kwargs['data'], str) and kwargs['headers'].get(
                'Content-Type') == 'application/json':
            kwargs['data'] = json.dumps(kwargs.get('data', {}))
        return getattr(super(), method)(**kwargs)

    def get_default_session_header(self) -> dict[str: str]:
        header = {'User-Agent': 'Mozilla/5.0',
                  'Authorization': self._api_key}
        return header

    def get_post_session_header(self) -> dict[str: str]:

        header = self.get_default_session_header()
        header['Content-Type'] = 'application/json'

        return header
