import requests


class Material():

    @classmethod
    def load_materials(cls, connection):
        res = connection.get('/api/materials/', verify=connection.verify_ssl)
        if res.status_code != 200:
            raise requests.exceptions.ConnectionError(
                f'Connection failed! {connection.host_url}: ({res.status_code})\n{res.text}')
        return [cls(e) for e in res.json()]

    def __init__(self, data):
        self.values = data
        self.id = data.get('id')

class EmptyMaterial():
    pass