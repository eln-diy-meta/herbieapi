import re
from datetime import datetime
from urllib.parse import urlparse

from herbie_api.connection import Connection
from herbie_api.open_api import OpenApi
from herbie_api.open_api.schema import ProtocolSchema
from requests.exceptions import RequestException


class SubElementList(list):
    def __init__(self, connection: Connection, protocol_type: str, open_api: OpenApi, create_url: str, parent_key: str, protocol_schema: ProtocolSchema):
        super().__init__()
        self._connection = connection
        self.protocol_type = protocol_type
        self._open_api = open_api
        self._create_url = create_url
        self._parent_key = parent_key
        self._protocol_schema = protocol_schema

    def add_new(self):
        p = Element(self._connection, self.protocol_type, self._open_api)
        p.set_parent_url(self._create_url)
        p.set_parent_key(self._parent_key)
        p.set_schema(self._protocol_schema)
        p.create_not_save()
        self.append(p)
        return p

    def add_load(self, id):
        p = Element(self._connection, self.protocol_type, self._open_api)
        p.set_id(id)
        p.set_schema(self._protocol_schema)
        p.load()
        self.append(p)
        return p

    def add_populate(self, json_data):
        p = Element(self._connection, self.protocol_type, self._open_api)
        p.set_schema(self._protocol_schema)
        p.populate(json_data)
        self.append(p)
        return p

    def remove(self, idx: int):
        self[idx].delete()
        del self[idx]





class Protocol:
    """
    A Protocol chemotion_instance represents the fundamental protocol items in Herbie.

    :param connection: The connection object initialized by a Herby chemotion_instance object
    :param protocol_type: the name of the protocol type
    """

    def __init__(self, connection: Connection, protocol_type: str, open_api: OpenApi):
        self._connection = connection
        self._open_api = open_api
        self.meta = None
        self._protocol_json_data = None
        self._element = Element(connection, protocol_type, open_api)
        self.id = None
        self.is_draft = False


    @property
    def protocol_type(self):
        return self._element.protocol_type

    @property
    def values(self):
        return self._element.values

    @property
    def material(self):
        return self._element.material

    def set_id(self, id: int):
        self.id = id

    def create(self):
        res = self._connection.post('/api/protocols/', data={'protocol_type': {'@id': self.protocol_type}})
        if res.status_code != 201 and res.status_code != 200:
            raise RequestException(f'Error creating {self.protocol_type} protocol! Error code: {res.status_code}')
        self._protocol_json_data = res.json()
        self._parse_json_to_meta()
        self._element.populate(self._get_meta_current_version().get('data'))


    def load(self):
        res = self._connection.get(f'/api/protocols/{self.id}/')
        if res.status_code != 200:
            raise RequestException(f'Error creating {self.protocol_type} protocol! Error code: {res.status_code}')
        self._protocol_json_data = res.json()
        self._parse_json_to_meta()
        self._element.populate(self._get_meta_current_version().get('data'))


    def populate(self, json_values: dict):
        self._protocol_json_data = json_values
        self._parse_json_to_meta()
        self._element.populate(self._get_meta_current_version().get('data'))

    def save_draft(self):
        res = self._connection.put(f'/api/protocols/{self.id}/draft/', data=self._parse_meta_to_json())
        if res.status_code != 200:
            raise RequestException(f'Saving draft error {self.id} protocol! Error code: {res.status_code}\n --> {res.text}')
        self._element.save()
        self.load()

    def publish(self):
        res = self._connection.post(f'/api/protocols/{self.id}/publish/')
        if res.status_code != 200:
            raise RequestException(
                f'Publishing draft error {self._element.protocol_type} protocol! Error code: {res.status_code}\n{res.text}')

    def to_draft(self):
        if self.is_draft: return
        res = self._connection.post(f'/api/protocols/{self.id}/edit/')
        if res.status_code != 200:
            raise RequestException(f'Make to draft error {self._element.protocol_type} protocol! Error code: {res.status_code}')
        self.load()

    def delete(self):
        res = self._connection.post(f'/api/protocols/{self.id}/cancel/')
        if res.status_code != 200:
            raise RequestException(f'Error deleting {self._element.protocol_type} protocol! Error code: {res.status_code}')
        self._protocol_json_data = None


    def _get_meta_current_version(self):
        obj = self._protocol_json_data.get('version_draft')
        self.is_draft = obj is not None
        if not self.is_draft:
            obj = self._protocol_json_data.get('version_published', {})
        return obj

    def parse_values_to_json(self):
        return self._parse_meta_to_json()

    def _parse_json_to_meta(self):
        v = self._get_meta_current_version()
        if 'id' in self._protocol_json_data:
            self.set_id(self._protocol_json_data['id'])
        self.meta = {
            "performance_times": [x | {'date': datetime.strptime(x['date'], "%Y-%m-%d").date()} for x in
                                  v.get('performance_times', [])],
            "clients_author": [x.get('author') for x in v.get('clients', []) if x.get('author') is not None],
            "clients_department": [x.get('department') for x in v.get('clients', []) if
                                   x.get('department') is not None],
            "clerks": v.get('clerks', []),
            "client_project_slugs": [],
            "external_clerk": v.get('external_partner', ""),
            "external_partner": v.get('external_partner', ""),
            "technical_center_involved": v.get('technical_center_involved', False),
            "technical_center_technician": v.get('technical_center_technician', "")

        }

    def parse_meta_to_json(self) -> dict|None:
        if self.meta is not None:
            return self._parse_meta_to_json()
        return None

    def _parse_meta_to_json(self) -> dict:
        return {
            "protocol_type": {"@id": self.protocol_type},
            "performance_times": [x | {'date': x['date'] if isinstance(x['date'], str) else x['date'].strftime("%Y-%m-%d")} for x in self.meta['performance_times']],
            "client_internal_usernames": [x['user']['username'] for x in self.meta['clients_author']],
            "client_department_abbreviations": [x['abbreviation'] for x in self.meta['clients_department']],
            "client_project_slugs": self.meta['client_project_slugs'],
            "external_partner": self.meta['external_partner'],
            "external_clerk": self.meta['external_clerk'],
            "clerk_usernames": [x['user']['username'] for x in self.meta['clerks']],
            "technical_center_involved": self.meta['technical_center_involved'],
            "technical_center_technician": self.meta['technical_center_technician']
        }


class Element:

    def __init__(self, connection: Connection, protocol_type: str, open_api: OpenApi):
        self._connection = connection
        self._schema = None
        self._open_api = open_api
        self.protocol_type = protocol_type.removeprefix('data-')
        self._element_json_data = None
        self.values = None
        self.id = None
        self.material = None
        self._base_create_url = f'/api/data-{self.protocol_type.removesuffix("-draft").removesuffix("-post")}s/'
        self._parent_url = None
        self._url = ''
        self._parent_key = None
        self._is_created = False

    def get_sub_element(self, protocol_type):
        e = Element(self._connection, protocol_type, self._open_api)
        e.set_parent_url(self.url)
        return e


    def set_id(self, id: int):
        self.id = id



    def set_schema(self, schema: ProtocolSchema):
        self._schema = schema

    def set_parent_url(self, parent_url: str):
        self._parent_url = parent_url

    def set_parent_key(self, parent_key: str):
        self._parent_key = parent_key


    @property
    def url(self):
        return self._url

    @property
    def create_url(self):
        if self._parent_url is not None:
            return self._parent_url
        return  self._base_create_url

    def create_not_save(self):
        if self.values is None:
            self._element_json_data = {}
            self._populate()

    def create(self):
        self.create_not_save()
        res = self._connection.patch(url_path=self.create_url, data={self._parent_key: [self.parse_values_to_json()]})
        if res.status_code != 201 and res.status_code != 200:
            raise RequestException(f'Error creating {self.protocol_type} protocol! Error code: {res.status_code}\n --> {res.text}')
        if self._parent_url is None:
            temp_json = res.json()
            self._element_json_data = temp_json
            self._populate()

    def delete(self):
        res = self._connection.delete(url_path=self.url)
        if res.status_code != 204:
            raise RequestException(f'Error deleteing {self.protocol_type} protocol! Error code: {res.status_code}\n --> {res.text}')
        self.id = None

    def load(self):
        res = self._connection.get(url_path=self.url)
        if res.status_code != 200:
            raise RequestException(f'Error fetching {self.id} protocol! Error code: {res.status_code}')
        self._element_json_data = res.json()
        self._populate()

    def save(self):
        if not self._is_created:
            return self.create()

        vals = self._parse_values_to_json(True)
        res = self._connection.patch(url_path=self.url,
                                     data=vals)
        if res.status_code != 200:
            raise RequestException(
                f'Saving draft error {self.protocol_type} protocol! Error code: {res.status_code}\n{res.text}')

    def populate(self, json_data):
        self._element_json_data = json_data
        self._populate()

    def _populate(self):
        self.id = self._element_json_data.get('id')
        self._url = urlparse(self._element_json_data.get('url')).path
        self._is_created = self.id is not None
        if self._schema is None:
            self._schema = self._open_api.get_protocol_schema(self.url)
        self._parse_json_to_values()


    def _parse_json_to_values(self):
        json_values = self._element_json_data
        self.values = self._open_api.parse_open_api(json_values, self._schema)

        for (key, item) in self.values.items():
            if isinstance(item, dict):
                if 'herbie_api_sub_schema' in item:
                    protocol_name = item['herbie_api_sub_schema']._draft_name
                    protocol_type = re.sub(r'(?<!^)(?=[A-Z])', '-', protocol_name).lower()
                    if isinstance(item['data'], list):
                        self.values[key]  = SubElementList(self._connection, protocol_type, self._open_api, self.url, self._schema.mapping[key]['target'], self._open_api.get_protocol_schema_from_draft_name(protocol_name))
                        for elm in item['data']:
                            self.values[key].add_populate(elm)
                    else:
                        self.values[key] = self.get_sub_element(protocol_type)
                        self.values[key].set_parent_key(self._schema.mapping[key]['target'])
                        self.values[key].set_schema(self._open_api.get_protocol_schema_from_draft_name(protocol_name))
                        if item.get('data') is not None:
                            self.values[key].populate(item['data'])

        if 'material_new' in json_values:
            self.material = json_values.get('material_new')
        elif 'rea_product' in json_values:
            self.material = json_values.get('rea_product')



    def parse_values_to_json(self) -> dict:
        return self._parse_values_to_json(False)

    def _parse_values_to_json(self, commit) -> dict:
        result = {}
        for key, mapping_details in self._schema.mapping.items():
            value = self.values[key]
            if mapping_details['exact']:
                if isinstance(value, Element):
                    if commit:
                        value.save()
                    else:
                        result[mapping_details['target']] = value.parse_values_to_json()
                if isinstance(value, SubElementList):
                    if commit:
                        for v in value:
                            v.save()
                    else:
                        result[mapping_details['target']] = [v.parse_values_to_json() for v in value]

                else:
                    result[mapping_details['target']] = value
            elif isinstance(value, Element) or isinstance(value, Protocol):
                if mapping_details.get('sub_element') == 'MaterialNested' and value.material is not None:
                    result[mapping_details['target']] = value.material.get('id')
                else:
                    result[mapping_details['target']] = value.id
            elif isinstance(value, dict):
                result[mapping_details['target']] = value[mapping_details['suffix']] if value is not None else None
            else:
                result[mapping_details['target']] = value

        return result