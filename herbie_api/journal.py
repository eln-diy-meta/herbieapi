from herbie_api.protocol import Protocol
from herbie_api.authors import AuthorManager
from herbie_api.connection import Connection

class Journal:

    def __init__(self, connection: Connection, open_api):
        self._connection = connection
        self._entries = None
        self._notes = None
        self._open_api = open_api

    @property
    def entries(self):
        if self._entries is None:
            self.load()
        return self._entries

    @property
    def notes(self):
        if self._notes is None:
            self.load()
        return self._notes

    def load(self):
        self._entries = []
        self._notes = []
        res = self._connection.get(url_path='/api/journal/', data={'': AuthorManager.get_me(self._connection)['user']['username']})
        if res.status_code != 201 and res.status_code != 200:
            raise ConnectionError(f'Could not load journal {res.status_code}\n --> {res.text}')

        journal_list = res.json()['tagged_pointers']
        for elem in journal_list:
            if elem['tag'] == 'protocol':
                res = self._connection.get(f"/api/protocols/{elem['pointer']['pk']}/")
                if res.status_code != 201 and res.status_code != 200:
                    raise ConnectionError(f'Could not load journal {res.status_code}\n --> {res.text}')
                json_values = res.json()
                protocol_type = json_values['version_published']['data']['protocol_type']['@id']
                p = Protocol(self._connection, protocol_type, self._open_api)
                p.populate(json_values)
                self._entries.append(p)
            elif elem['tag'] == 'note':
                res = self._connection.get(f"/api/entry-texts/{elem['pointer']['pk']}/")
                if res.status_code != 201 and res.status_code != 200:
                    raise ConnectionError(f'Could not load journal {res.status_code}\n --> {res.text}')
                self._notes.append(res.json())